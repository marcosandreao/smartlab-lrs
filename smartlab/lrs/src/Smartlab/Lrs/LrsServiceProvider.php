<?php

namespace Smartlab\Lrs;

use DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\ServiceProvider;

class LrsServiceProvider extends ServiceProvider {
	private $smartlab;
	public function __construct($app) {
		$this->smartlab = new Smartlab ();
	}
	
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;
	
	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot() {
		$this->package ( 'smartlab/lrs' );
	}
	
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {
		DB::listen ( function ($sql, $bindings, $time) {
			$this->$smartlab->handlerSql ( $sql, $bindings, $times );
		} );
	}
	
	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides() {
		return array ();
	}
}
