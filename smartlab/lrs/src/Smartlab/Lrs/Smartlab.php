<?php

namespace Smartlab\Lrs;

use Log;

class Smartlab implements ISmartlab {
	public function handlerSql($sql, $bindings, $times) {
		Log::debug ( "" . $sql );
		Log::debug ( json_encode ( $bindings ) );
	}
}